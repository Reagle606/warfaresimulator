using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollManager : MonoBehaviour
{
	private Rigidbody[] ragdollParts;
	public AimIK aimIk;


	private void Awake()
	{
		ragdollParts = GetComponentsInChildren<Rigidbody>();
		EnableRagdoll(false);
	}
	public void EnableRagdoll(bool value)
	{
		GetComponent<Animator>().enabled = !value;
		foreach(Rigidbody rb in ragdollParts)
		{
			rb.useGravity = value;
			rb.isKinematic = !value;
		}
		aimIk.enabled = !value;
	
	}
}
