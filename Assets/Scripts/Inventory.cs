using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inventory 
{
	public InventorySlot[] slots;
	public InventorySlot[] internalSlots;

	[SerializeField]
	private int internalSlotCount;

	public Transform inventoryParent;

	public Inventory(Inventory _inventory)
	{
		slots = new InventorySlot[_inventory.slots.Length];
		for (int i = 0; i < _inventory.slots.Length; i++)
		{
			slots[i] = new InventorySlot(_inventory.slots[i]);
		}

		internalSlots = new InventorySlot[internalSlotCount];
		for(int i = 0; i < internalSlotCount; i++)
		{
			internalSlots[i].SlotID = i.ToString();
		}
	}

	public void Init(Transform _inventoryParent)
	{
		inventoryParent = _inventoryParent;

		foreach (InventorySlot s in slots)
		{
			InventorySlotLocator locator = InventoryUtils.GetSlotLocator(inventoryParent.transform, s.SlotID);
			if (locator != null)
				s.AddSlotLocator(locator);
		}
	}

	/// <summary>
	/// Gets the item in a specified Slot
	/// </summary>
	/// <param name="SlotID"></param>
	/// <returns></returns>
	public Item GetSlotItem(string SlotID)
	{
		foreach (InventorySlot s in slots)
		{
			if (s.SlotID == SlotID)
			{
				return s.item;
			}
		}
		Debug.Log($"The Slot {SlotID} could not be found");
		return null;
	}

	/// <summary>
	/// Gets The specified Slot
	/// </summary>
	/// <param name="SlotID"></param>
	/// <returns></returns>
	public InventorySlot GetSlot(string SlotID)
	{
		foreach(InventorySlot s in slots)
		{
			if(s.SlotID == SlotID)
			{
				return s;
			}
		}
		Debug.Log($"The Slot {SlotID} could not be found");
		return null;
	}



}
