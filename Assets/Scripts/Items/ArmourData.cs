using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = ("Items/Armour/Armour"))]
public class ArmourData : ItemData
{
	public Inventory inventoryData;
}

public class Armour : Item
{
	public ArmourInstance ArmourInstance { get { return (ArmourInstance)itemInstance; } }
	public ArmourData ArmourData { get { return (ArmourData)itemData; } }

	public Inventory inventory;

	public Armour(ArmourData data) : base(data)
	{
		inventory = new Inventory(data.inventoryData);
	}
}
