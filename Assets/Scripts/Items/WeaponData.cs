using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = ("Items/Weapons/Weapon"))]
public class WeaponData : ItemData
{
	public float fireRate;

	[System.Serializable]
	public class ReloadSequence
	{
		public ReloadStates reloadState;
		public float reloadTime;
	}

	[SerializeField]
	public ReloadSequence[] reloadSequence;

	public ReloadSequence GetReloadSequence(ReloadStates state)
	{
		foreach (ReloadSequence seq in reloadSequence)
		{
			if (seq.reloadState == state)
			{
				return seq;
			}
		}
		return null;
	}
}

public class Weapon : Item
{

	public WeaponInstance WeaponInstance { get { return (WeaponInstance)itemInstance; } }
	public WeaponData WeaponData { get { return (WeaponData)itemData; } }

	public Weapon(WeaponData data) : base(data)
	{
		GetProjectile();
		magazineSlot.onItemAdded += OnMagazineLoaded;
		magazineSlot.onItemRemoved += OnMagazineUnloaded;
	}

	public bool roundReadyToFire { get { return roundChambered && !boltOpen; } }
	public bool roundChambered;
	public bool shellChambered;
	public bool boltOpen = false;

	private float boltClosedPercent;
	public float BoltClosedPercent { get { return boltClosedPercent; } }

	private bool autoLoad;
	private float autoLoadTimer;

	public InventorySlot magazineSlot = new InventorySlot("Magazine_Slot");

	public Magazine loadedMagazine { get; private set; }

	public Projectile currentProjectile;


	public void GetProjectile()
	{
		currentProjectile = ProjectileManager.instance.defaultProjectile;
	}

	private void OnMagazineLoaded(Item i)
	{
		if (i != null)
			loadedMagazine = i as Magazine;

		if (loadedMagazine != null && !loadedMagazine.ammo.IsEmpty && boltOpen)
			LoadRound();

	}
	private void OnMagazineUnloaded()
	{
		loadedMagazine = null;
	}
	public override void PrimaryUse()
	{
		base.PrimaryUse();
		if (roundReadyToFire)
		{
			WeaponInstance.PrimaryUse();
			autoLoad = true;
			autoLoadTimer = 0;
			roundChambered = false;
		}
		
	}

	public void UpdateWeapon()
	{
		if (autoLoad)
		{
			autoLoadTimer += Time.deltaTime;
			if (autoLoadTimer >= WeaponData.fireRate / 2)
			{
				OpenBolt(true);
			}
			if (autoLoadTimer >= WeaponData.fireRate)
			{
				autoLoad = false;
				autoLoadTimer = 0;
				OpenBolt(false);
			}
		}
	}


	public void OpenBolt(bool value)
	{
		bool wasOpen = boltOpen;
		boltOpen = value;
		
		if (boltOpen && !wasOpen)
		{
			EjectRound();
			if (loadedMagazine != null && !loadedMagazine.ammo.IsEmpty)
				LoadRound();
		}
		else if(!boltOpen && wasOpen)
		{
			if (roundChambered)
			{
				shellChambered = true;
			}
		}
		boltClosedPercent = value ? 0 : 1;
	}
	public void ChamberRound()
	{
		roundChambered = true;
		shellChambered = true;
	}
	public void LoadRound()
	{
		roundChambered = true;
		if(loadedMagazine != null)
			loadedMagazine.ammo.AdjustValue(-1);
	}
	public void EjectRound()
	{
		if (roundChambered)
		{
			roundChambered = false;
		}
		if (shellChambered)
		{
			shellChambered = false;
			WeaponInstance.PlayBulletShellFX();
		}
	}
}


