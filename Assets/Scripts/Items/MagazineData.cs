using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyResources;

[CreateAssetMenu(menuName = ("Items/Magazines/Magazine"))]
public class MagazineData : ItemData
{
	public int maxAmmo;
}

public class Magazine : Item
{
	public MagazineInstance MagazineInstance { get { return (MagazineInstance)itemInstance; } }
	public MagazineData MagazineData { get { return (MagazineData)itemData; } }

	public Resource ammo = new Resource();

	public Magazine(MagazineData data) : base(data)
	{
		ammo.max = data.maxAmmo;
		ammo.SetPercentage(100);
	}

}



