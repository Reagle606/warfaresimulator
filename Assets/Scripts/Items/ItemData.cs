using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EasyResources;

public class ItemData : ScriptableObject
{
	[SerializeField]
	public ItemInstance itemInstancePrefab;
	
}

public class Item
{
	protected readonly ItemData itemData = null;
	public ItemData ItemData { get { return itemData; } }

	protected ItemInstance itemInstance;
	public ItemInstance ItemInstance {  get { return itemInstance; } }

	private InventorySlot parentSlot;
	public InventorySlot ParentSlot { get { return parentSlot; } }


	public Entity holder { get; private set; }


	public void SetHolder(Entity _holder)
	{
		holder = _holder;
	}

	public void SetParentSlot(InventorySlot slot)
	{
		parentSlot = slot;
	}

	public Item(ItemData data)
	{
		itemData = data;
	}

	public virtual void SpawnItemInstance(Vector3 pos, Quaternion rot, Transform parent = null)
	{
		ItemInstance inst = GameObject.Instantiate(itemData.itemInstancePrefab, pos, rot, parent);
		itemInstance = inst;
		inst.Init(this);

	}
	public virtual void PrimaryUse()
	{
		
	}
}



