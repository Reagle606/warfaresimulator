using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponInstance : ItemInstance
{
	public Weapon Weapon { get { return (Weapon)item; } }

	public Transform projectileFirePoint;
	public ParticleSystem bulletShellParticle;

	public Transform bolt;
	public Vector3 boltOpenPos;
	public Vector3 boltClosedPos;


	public override void Init(Item item)
	{
		base.Init(item);
		Weapon.magazineSlot.AddSlotLocator(InventoryUtils.GetSlotLocator(this.transform, Weapon.magazineSlot.SlotID));
	}
	public override void PrimaryUse()
	{
		base.PrimaryUse();
	
		Instantiate(Weapon.currentProjectile, projectileFirePoint.position, projectileFirePoint.rotation);
	}

	public void Update()
	{
		if(Weapon != null)
		{
			Weapon.UpdateWeapon();
		}
		bolt.transform.localPosition = Vector3.Lerp(boltOpenPos, boltClosedPos, Weapon.BoltClosedPercent);
	}

	public void PlayBulletShellFX()
	{
		bulletShellParticle.Play();
	}

}
