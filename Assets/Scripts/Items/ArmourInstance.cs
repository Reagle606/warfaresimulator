using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmourInstance : ItemInstance
{
	public Armour Armour { get { return (Armour)item; } }

	public override void Init(Item item)
	{
		base.Init(item);
		Armour.inventory.Init(this.transform);
		
	}
}
