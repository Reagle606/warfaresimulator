using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInstance : MonoBehaviour
{
	protected Item item = null;
	public virtual Item Item { get { return item; } }

	public Collider collider;
	public Rigidbody rigidbody;

	public Transform RGrip, RPole;
	public Transform LGrip, LPole;

	public virtual void Init(Item item)
	{
		this.item = item;
		collider = GetComponent<Collider>();
		rigidbody = GetComponent<Rigidbody>();
		EnablePhysics(false);
	}

	public virtual void PrimaryUse()
	{

	}

	public void DropItem()
	{

	}

	public void EnablePhysics(bool value)
	{
		rigidbody.isKinematic = !value;
		rigidbody.useGravity = value;
		collider.enabled = value;
	}
}
