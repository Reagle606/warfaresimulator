using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

	[SerializeField] private float speed = 1;

	// Update is called once per frame
	void Update()
	{
		Vector3 nextFramePos = transform.position + transform.forward * speed;
		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, Vector3.Distance(transform.position, nextFramePos)))
		{
			transform.position = hit.point;
			Destroy(this.gameObject, 0.2f);
			return;
		}
		transform.position = nextFramePos;
	}
}
