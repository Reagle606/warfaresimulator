using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum ReloadStates
{
    None,
    Unload,
    StoreMag,
    GetNewMag,
    LoadMag,
    OpenBolt,
    CloseBolt
}