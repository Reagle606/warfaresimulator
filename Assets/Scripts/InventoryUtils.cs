using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class InventoryUtils
{
	public static bool AddItem(Item i, InventorySlot to)
	{
		if (i != null && to != null)
		{
			if (to.CanAddItem(i))
			{
				Item item = i;
				if (i.ParentSlot != null)
					i.ParentSlot.RemoveItem();
				to.AddItemInternal(item);
				return true;
			}
		}
		return false;
	}


	public static InventorySlotLocator GetSlotLocator(Transform parentObject, string SlotID)
	{
		InventorySlotLocator[] locators = parentObject.GetComponentsInChildren<InventorySlotLocator>();
		foreach (InventorySlotLocator sl in locators)
		{
			if (SlotID == sl.SlotID)
			{
				return sl;
			}
		}
		Debug.Log("Could not find slot locator of: " + SlotID + " on " + parentObject.name);
		return null;
	}

}
