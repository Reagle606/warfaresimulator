using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class Entity : MonoBehaviour
{
	[Header("Debug")]
	public ItemData currentItemData;
	public MagazineData debugMagazine;
	public ArmourData debugBelt;
	public ArmourData debugSidearmHolster;
	public ArmourData debugBackPack;
	public ArmourData debugHelmet;
	private Armour belt;
	private Armour sideArmHolster;
	private Armour backPack;

	[SerializeField]
	private Inventory inventory;
	public Inventory Inventory { get { return inventory; } }

	public RagdollManager ragdollManager { get; private set; }

	public Weapon rWeapon { get; private set; }

	public Transform rightHand;

	public void Awake()
	{
		ragdollManager = GetComponent<RagdollManager>();
		inventory.Init(this.transform);
	}
	public void Start()
	{
		inventory.GetSlot("Right_Grip").onItemAdded += ItemEquipped;
		inventory.GetSlot("Right_Grip").onItemRemoved += ItemUnequipped;
		DebugSpawnAllItems();
	}
	private void DebugSpawnAllItems()
	{
		belt = ItemManager.instance.CreateItem(debugBelt) as Armour;
		sideArmHolster = ItemManager.instance.CreateItem(debugSidearmHolster) as Armour;
		backPack = ItemManager.instance.CreateItem(debugBackPack) as Armour;
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(currentItemData), inventory.GetSlot("Right_Grip"));
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(debugMagazine), rWeapon.magazineSlot);
		InventoryUtils.AddItem(belt, inventory.GetSlot("Belt_Slot"));
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(debugMagazine), belt.inventory.GetSlot("Right"));
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(debugMagazine), belt.inventory.GetSlot("Front_Right"));
		InventoryUtils.AddItem(sideArmHolster, inventory.GetSlot("Right_Thigh"));
		InventoryUtils.AddItem(backPack, inventory.GetSlot("Back_Slot"));
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(currentItemData), backPack.inventory.GetSlot("Right"));
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(currentItemData), backPack.inventory.GetSlot("Left"));
		InventoryUtils.AddItem(ItemManager.instance.CreateItem(debugHelmet), inventory.GetSlot("Hat_Slot"));
		rWeapon.OpenBolt(true);
		rWeapon.OpenBolt(false);
	}


	public void Update()
	{
		DebugTimeScale();

		DebugBehave();
		if (Input.GetKeyDown(KeyCode.K))
		{
			Die();
		}

	}


	private void DebugBehave()
	{
		inventory.GetSlotItem("Right_Grip")?.PrimaryUse();

		if (rWeapon != null)
		{
			if (rWeapon.loadedMagazine != null)
			{
				if (rWeapon.loadedMagazine.ammo.IsEmpty)
				{
					UnloadMagazineAction();
					InventoryUtils.AddItem(belt.inventory.GetSlotItem("Right"), rWeapon.magazineSlot);
					rWeapon.OpenBolt(true);
					rWeapon.OpenBolt(false);

				}
			}
		}
	}

	float timeScale = 1;
	private void DebugTimeScale()
	{

	}

	#region Damage
	private void Die()
	{
		ragdollManager.EnableRagdoll(true);
		DropCurrentItem();
	}
	private void DropCurrentItem()
	{
		if (inventory.GetSlot("Right_Grip") != null)
		{
			inventory.GetSlot("Right_Grip").RemoveItem(true);
		}
	}
	#endregion
	#region Equipment
	public void EquipItem(Item i)
	{
		InventoryUtils.AddItem(i, inventory.GetSlot("Right_Grip"));
	}

	public void ItemEquipped(Item i)
	{
		if (i is Weapon)
			rWeapon = i as Weapon;
		i.SetHolder(this);
	}
	public void ItemUnequipped()
	{
		if (rWeapon != null)
			rWeapon.SetHolder(null);
		rWeapon = null;

	}

	#endregion

	#region Actions

	public Item UnloadMagazineAction()
	{
		return rWeapon.magazineSlot.RemoveItem(true);
	}

	#endregion


}
