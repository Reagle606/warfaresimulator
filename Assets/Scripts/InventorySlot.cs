using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InventorySlot
{
	public Item item = null;

	public string SlotID;
	public InventorySlotLocator slotLocation { get; private set; }

	public delegate void OnItemAdded(Item i);
	public OnItemAdded onItemAdded;

	public delegate void OnItemRemoved();
	public OnItemRemoved onItemRemoved;

	public InventorySlot(string ID)
	{
		SlotID = ID;
	}
	public InventorySlot(InventorySlot other)
	{
		SlotID = other.SlotID;
		slotLocation = other.slotLocation;
		item = other.item;
		onItemAdded = other.onItemAdded;
		onItemRemoved = other.onItemRemoved;
	}
	public bool CanAddItem(Item i)
	{
		if(item == null)
		{
			return true;
		}
		return false;
	}

	public void AddSlotLocator(InventorySlotLocator locator)
	{
		slotLocation = locator;
	}
	public void AddItemInternal(Item i)
	{
		if (i != null)
		{
			if (CanAddItem(i))
			{
				item = i;
				item.SetParentSlot(this);
				onItemAdded?.Invoke(i);
				if (slotLocation != null)
				{
					if (item.ItemInstance == null)
						item.SpawnItemInstance(slotLocation.transform.position, slotLocation.transform.rotation, slotLocation.transform);
					else
					{
						item.ItemInstance.transform.SetParent(slotLocation.transform);
						item.ItemInstance.transform.localPosition = Vector3.zero;
						item.ItemInstance.transform.localRotation = Quaternion.identity;
					}
				}
			}
		}
	}

	public Item RemoveItem(bool drop = false)
	{
		Item returnItem = null;
		if (item != null)
		{
			item.SetParentSlot(null);
			if (drop && item.ItemInstance != null)
			{
				item.ItemInstance.transform.SetParent(null);
				item.ItemInstance.EnablePhysics(true);
			}
			returnItem = item;
			onItemRemoved?.Invoke();
			item = null;
		}
		return returnItem;
	}
}
