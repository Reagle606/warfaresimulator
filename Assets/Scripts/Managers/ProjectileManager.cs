using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
	public static ProjectileManager instance;

	public void Awake()
	{
		instance = this;
	}

	public Projectile defaultProjectile;
}
