using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemManager : MonoBehaviour
{
	public static ItemManager instance;

	public void Awake()
	{
		instance = this;
	}

	public Item CreateItem(ItemData data)
	{
		switch (data.GetType().ToString())
		{
			case "WeaponData":
				return new Weapon((WeaponData)data);
			case "MagazineData":
				return new Magazine((MagazineData)data);
			case "ArmourData":
				return new Armour((ArmourData)data);
		}

		return new Item(data);
	}
}
